Parse.Cloud.define('new_game', function (request, response) {
    var user = request.user;
    var query = new Parse.Query('Game');
    query.equalTo('second', undefined);
    query.equalTo('is_running', false);

    query.count({
        success: function (count) {
            var game;
            if (count > 0) {
                game = query.first().then(function (result) {
                    result.set('second', user);
                    result.set('score_second', 0);
                    result.set('is_running', true);

                    result.save(null, {
                        success: function (g) {
                            response.success({game_id: g.id, turn: 'second'});
                        }
                    });
                });
                return;
            }

            var Game = new Parse.Object.extend('Game');
            game = new Game();
            game.set('first', user);
            game.set('score_first', 0);
            game.set('is_running', false);
            game.set('turn', 'first');

            game.save(null, {
                success: function (g) {
                    setTimeout(function () {
                        game.set('is_running', true);
                        game.save();
                        response.success({game_id: g.id, turn: 'first'});
                    }, 5000)
                }
            });
        }
    });

});


Parse.Cloud.afterSave('Game', function (request) {
    if (request.object.get('is_running') === false) {
        var first = request.object.get('first');
        var second = request.object.get('second');
        var score_first = 0;
        var score_second = 0;

        if (request.object.get('score_second') === 99) {
            score_first = 50;
            score_second = 150;
        }
        if (request.object.get('score_first') === 99) {
            score_first = 150;
            score_second = 50;
        }

        if (first !== undefined) {
            console.log(first);
            first.fetch().then(function (u) {
                var score1 = u.get('score');
                u.set('score', score1 + score_first);
                u.save(null, {useMasterKey: true});
            });

        }
        if (second !== undefined) {
            second.fetch().then(function(u){
                var score2 = u.get('score');
                u.set('score', score2 + score_second);
                u.save(null, {useMasterKey: true});
            });
        }
    }
});