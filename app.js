var express = require('express');
var app = express();

var ParseServer = require('parse-server').ParseServer;
var api = new ParseServer({
    databaseURI: 'mongodb://localhost:27017/dev',
    cloud: '/Users/ali/WebstormProjects/web_game_server/cloud.js',
    appId: 'myAppId',
    masterKey: 'myMasterKey',
    fileKey: 'optionalFileKey',
    javascriptKey: '1xoWtDkxw8oZvX3bzhdTuHU7KZB8SGZD9jWQ2V9p',
    serverURL: 'http://localhost:1337/parse',
    liveQuery: {
        classNames: ['Game'],
        redisURL: 'redis://localhost:6379'
    }
});
app.use('/parse', api);


var ParseDashboard = require('parse-dashboard');
var dashboard = new ParseDashboard({
    "apps": [
        {
            "serverURL": "http://localhost:1337/parse",
            "appId": "myAppId",
            "masterKey": "myMasterKey",
            "appName": "MyApp"
        }
    ]
});
app.use('/dashboard', dashboard);

var httpServer = require('http').createServer(app);
httpServer.listen(1337);

ParseServer.createLiveQueryServer(httpServer,  {
    appId: 'myAppId',
    keyPairs: {
        javascriptKey: '1xoWtDkxw8oZvX3bzhdTuHU7KZB8SGZD9jWQ2V9p',
        masterKey: 'myMasterKey'
    },
    serverURL: 'http://localhost:1337/parse',
    websocketTimeout: 10 * 1000,
    cacheTimeout: 60 * 600 * 1000,
    logLevel: 'VERBOSE',
    redisURL: 'redis://localhost:6379'
});

